fetch("https://swapi.dev/api/films/")
        .then(response => response.json())
        .then((res) => {

            res.results.forEach(function (elem) {
                let title = document.createElement('h2');
                title.innerText = elem.title;
                document.body.append(title);

                let crawls = document.createElement('h3');
                crawls.innerText = `Opening crawl: ${elem.opening_crawl}`;
                title.after(crawls);

                let episode = document.createElement('h3');
                episode.innerText = `Episode: ${elem.episode_id}`;
                title.after(episode);

                let urls = elem.characters;
                let requests = urls.map(url => fetch(url));
                spinner ();


                Promise.all(requests)
                    .then(responses => responses.forEach(
                        response => response.json()
                            .then((res) => {
                                let para = document.createElement('p');
                                para.innerText = res.name;
                                title.after(para);
                                spinnerOff();
                            })
                    ));

            })
        })



function spinner () {
    let spinner = document.querySelector('.spinner');
    spinner.classList.add('spin');
}

function spinnerOff () {
    let spinner = document.querySelector('.spinner');
    spinner.classList.remove('spin');
}






